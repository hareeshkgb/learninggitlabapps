//
//  SecondViewController.swift
//  LearningGitLabApps
//
//  Created by Hareesh Gangadhara on 17/10/20.
//  Copyright © 2020 Hareesh Gangadhara. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    func callmeOnce()
    {
        print("I am SecondViewcontroller function!!!")
        print("I am second comment from SecondViewcontroller...")
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
